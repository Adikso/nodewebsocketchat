var users = [];

class User {

	set name(name) {
		this._name = name;
	}

	get name() {
		return this._name;
	}

	set client(client) {
		this._client = client;
	}

	get client() {
		return this._client;
	}

	disconnect() {
		this._client.terminate();
	}

}

module.exports = {
	class: User,

	list: function() {
		return users;
	},

	get: function(name) {
		for (var i = 0; i < users.length; i++){
			var user = users[i];

			if (user.name === name) {
				return user;
			}
		}

		var newUser = new User();
		newUser.name = name;
		users.push(newUser);

		return newUser;
	},

	getByClient: function(client) {
		for (var i = 0; i < users.length; i++){
			var user = users[i];

			if (user.client === client) {
				return user;
			}
		}
	},

	exists: function(name) {
		for (var i = 0; i < users.length; i++){
			var user = users[i];

			if (user.name === name) {
				return true;
			}
		}

		return false;
	},

	isLoggedIn: function(client) {
		for (var i = 0; i < users.length; i++){
			var user = users[i];

			if (user.client === client) {
				return true;
			}
		}

		return false;
	}
}