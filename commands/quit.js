module.exports = {
	name: function () {
		return "quit";
	},

	process: function (commandArguments) {
		return function (Channels, Users, Commands, Client) {
			var user = Users.getByClient(Client);
			user.disconnect();

			Client.terminate();
		}
	}
}