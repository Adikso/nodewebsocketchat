module.exports = {
	name: function () {
		return "users";
	},

	process: function (commandArguments) {
		return function (Channels, Users, Commands, Client) {
			if (!Users.isLoggedIn(Client)){
				Client.send("You have to be logged in");
				return;
			}

			var user = Users.getByClient(Client);
			var channel = Channels.get(commandArguments[0]);

			var userNames = [];

			channel.users.forEach(function each(user) {
				userNames.push(user.name);
			});

			var userNamesString = userNames.toString();
			Client.send(`Online users: ${userNamesString}`);
		}
	}
}