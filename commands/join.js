module.exports = {
	name: function () {
		return "join";
	},

	process: function (commandArguments) {
		return function (Channels, Users, Commands, Client) {
			if (!Users.isLoggedIn(Client)){
				Client.send("You have to log-in before joining channels");
				return;
			}

			var user = Users.getByClient(Client);
			var channel = Channels.get(commandArguments[0]);

			Channels.quitAllChannels(user);
			channel.addUser(user);

			Client.send(`There are ${channel.users.length} users on this channel`);

			channel.users.forEach(function each(userTarget) {
				userTarget.client.send(`${user.name} joined the channel`);
			});

			console.log(`${user.name} joined #${channel.name}`);
		}
	}
}