module.exports = {
	name: function () {
		return "login";
	},

	process: function (commandArguments) {
		return function (Channels, Users, Commands, Client) {
			var username = commandArguments[0];

			if (Users.exists(username)){
				Client.send(`User '${username}' is already logged in. Choose different nickname`);
				return;
			}

			var user = Users.get(commandArguments[0]);
			user.client = Client;

			Client.send("You are now logged in!");
			Client.send("Type '/join [channelName]' to join channel");

			console.log(`${user.name} logged in!`);
		}
	}
}