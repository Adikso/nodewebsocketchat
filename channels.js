var channels = [];

class Channel {

	constructor(name) {
		this._users = [];
		this._name = name;
	}

	set name(name) {
		this._name = name;
	}

	get name() {
		return this._name;
	}

	get users() {
		return this._users;
	}

	addUser(user) {
		this._users.push(user);
	}

	removeUser(user) {
		this._users.splice( this._users.indexOf(user), 1);
	}

	hasUser(user) {
		return this._users.indexOf(user) !== -1;
	}

	sendMessage(message) {
		this._users.forEach(function each(userTarget) {
			userTarget.client.send(message);
		});

		console.log(message);
	}

}

module.exports = {
	class: Channel,

	list: function() {
		return channels;
	},

	get: function(name) {
		for (var i = 0; i < channels.length; i++) {
			var channel = channels[i];

			if (channel.name === name) {
				return channel;
			}
		}

		var newChannel = new Channel(name);
		channels.push(newChannel);

		return newChannel;
	},

	quitAllChannels: function(user) {
		channels.forEach(function each(channel) {
			if (channel.hasUser(user)) {
				channel.removeUser(user);
			}
		});
	},

	getUserChannels: function(user) {
		var userChannels = [];

		for (var i = 0; i < channels.length; i++) {
			var channel = channels[i];

			if (channel.hasUser(user)) {
				userChannels.push(channel);
			}
		}

		return userChannels;
	}
}