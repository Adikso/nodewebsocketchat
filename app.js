const WebSocket = require('ws');

const Channels = require('./channels.js');
const Users = require('./users.js');
const Commands = require('./commands.js');

Commands.loadAll();

const wss = new WebSocket.Server({ port: 8080 });

function heartbeat() {
	this.isAlive = true;
}

wss.on('connection', function connection(ws) {

	ws.send("Welcome to the Chat Server!");
	ws.send("You have to log-in first with '/login [nickname]'");

	ws.on('message', function incoming(message) {
		var callback = Commands.process(message);

		if (callback !== undefined) {
			callback(Channels, Users, Commands, ws);
			return;
		}

		var user = Users.getByClient(ws);
		var channels = Channels.getUserChannels(user);

		channels.forEach(function each(channel) {
			channel.sendMessage(`${user.name}: ${message}`);
		});
		
	});

	ws.on('close', function close() {
		var user = Users.getByClient(ws);
		var channels = Channels.getUserChannels(user);

		channels.forEach(function each(channel) {
			channel.removeUser(user);
			channel.sendMessage(`${user.name} disconnected (Reason: closed)`);
		});
	});

	ws.isAlive = true;
	ws.on('pong', heartbeat);
});

const interval = setInterval(function ping() {
	wss.clients.forEach(function each(ws) {
		if (ws.isAlive === false){
			var user = Users.getByClient(ws);
			var channels = Channels.getUserChannels(user);

			channels.forEach(function each(channel) {
				channel.removeUser(user);
				channel.sendMessage(`${user.name} disconnected (Reason: Timed out)`);
			});

			return ws.terminate();
		}

		ws.isAlive = false;
		ws.ping('', false, true);
	});
}, 30000);