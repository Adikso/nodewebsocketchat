const commandsFolder = './commands/';
const fs = require('fs');

var commands = [];

module.exports = {
	loadAll: function() {
		fs.readdirSync(commandsFolder).forEach(file => {
		  commands.push(require(commandsFolder + file));
		})
	},

	load: function(filename) {
		require(commandsFolder + filename);
	},

	process: function(command) {
		if (command.indexOf(" ") === -1){
			commandName = command.substring(1);
		}else{
			commandName = command.substring(1, command.indexOf(" "));
			commandArguments = command.substring(command.indexOf(" ") + 1).split(" ");
		}

		for (var i = 0; i < commands.length; i++) {
			var command = commands[i];

			if (command.name() == commandName) {
				return command.process(commandArguments);
			}
		}

		return undefined;
	}
}